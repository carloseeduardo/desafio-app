sap.ui.define([
	"sap/ui/core/mvc/Controller"
],
	/**
	 * @param {typeof sap.ui.core.mvc.Controller} Controller
	 */
	function (Controller) {
		"use strict";

		return Controller.extend("desafioapp.controller.View1", {
			onInit: function () {
                this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
		    }
		});
	});
