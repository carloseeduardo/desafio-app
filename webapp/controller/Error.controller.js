sap.ui.define([
    "sap/ui/core/mvc/Controller"
],
    function (Controller) {
        "use strict";
        return Controller.extend("desafioapp.controller.Detail", {
            onNavBack: function() {
                const oRouter = this.getOwnerComponent().getRouter();
				oRouter.navTo("overview");
            }
    });
});