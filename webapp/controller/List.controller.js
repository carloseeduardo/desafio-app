sap.ui.define([
    "sap/ui/core/mvc/Controller",
    'sap/ui/core/Fragment',
	"sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/Sorter",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/BusyIndicator"
],
    function (Controller, Fragment, Filter, FilterOperator,Sorter , JSONModel, BusyIndicator) {
        "use strict";

        return Controller.extend("desafioapp.controller.List", {
            onInit : function(){
                const oModel = new sap.ui.model.odata.v2.ODataModel('/V3/Northwind/Northwind.svc');

                oModel.read("/Orders", {
                    success: (response) => {
                        
                        const aResults = response.results  
                        const fModifyResults = (aOrders)=>{
                            const initAddedData = (oOrder)=>{
                                if (!oOrder.Added){
                                    oOrder.Added = {}
                                }
                                return oOrder
                            }
                            const setStatus = (oOrder)=>{
                                const lateThreshold  = 3,
                                    urgentThreshold = 16,
                                    requirementDate = oOrder.RequiredDate,
                                    shippedDate = oOrder.ShippedDate,
                                    iDateDiffInMs = (requirementDate - shippedDate),
                                    iMsInADay = 1000*60*60*24,
                                    iDateDiffInDays = iDateDiffInMs/iMsInADay

                                if (iDateDiffInDays > urgentThreshold){
                                    oOrder.Added.Status = "Success"
                                    oOrder.Added.StatusText = "In time"
                                } else if(iDateDiffInDays > lateThreshold){
                                    oOrder.Added.Status = "Warning"
                                    oOrder.Added.StatusText = "Urgent"
                                } else {
                                    oOrder.Added.Status = "Error"
                                    oOrder.Added.StatusText = "Too Late"
                                }
                                return oOrder
                            }
                            
                            aOrders.forEach((order)=>{
                                order = initAddedData(order)
                                order = setStatus(order)
                            });
                            return aOrders
                        }
                        
                        const aModifiedResults = fModifyResults(aResults),
                            listModel = new JSONModel(aModifiedResults);
                        
                        this.getView().setModel(listModel,'ordersList');
                        
                        
                        this.filterItems(aResults);
                    },
                    error: (error) => {
                        const oRouter = this.getOwnerComponent().getRouter();
                        oRouter.navTo('error');
                    }
                })

                this._mDialogs = {};
            },

            filterItems : function(data) {
                const filterModel = data.reduce((newObj, item) => {
                    Object.entries(item).forEach((_e) => {
                        if(typeof _e[1] !== 'object' && _e[1] !== null)
                            newObj[_e[0]] ? newObj[_e[0]].push(_e[1]) : newObj[_e[0]] = [_e[1]]
                    })
                    return newObj;
                }, {});

                const filteredItems = {};

                Object.entries(filterModel).map((e) => {
                    filteredItems[`${e[0]}`] = e[1].filter(function(item, pos, self) {
                        return self.indexOf(item) == pos;
                    });
                });

                this.getView().setModel(new JSONModel(filteredItems),'filteredItems');
            },

            hideBusyIndicator : function() {
	    		BusyIndicator.hide();
            },
        
            showBusyIndicator : function (iDuration, iDelay) {
                BusyIndicator.show(iDelay);

                if (iDuration && iDuration > 0) {
                    if (this._sTimeoutId) {
                        clearTimeout(this._sTimeoutId);
                        this._sTimeoutId = null;
                    }

                    this._sTimeoutId = setTimeout(function() {
                        this.hideBusyIndicator();
                    }.bind(this), iDuration);

                }
		    },

            onPress: function (oEvent) {
                var oItem = oEvent.getSource();
                var oRouter = this.getOwnerComponent().getRouter();
                oRouter.navTo("detail", {
                    orderPath: window.encodeURIComponent(oItem.getTitle().split(' ')[1])
                });
            },
        
            onSearch : function (oEvent) {    
                const query = oEvent.getParameter("query");
                const binding = this.byId('orderList').getBinding("items");
                var testNan = isNaN(Number(query));
                

                  if (!query) {
                    binding.filter([]);
                } else if (isNaN(Number(query))) {
                    binding.filter([new Filter([
                        new Filter("ShipName", FilterOperator.Contains, query)
                    ],false)]
                )} else {
                    binding.filter([new Filter([
                        new Filter("OrderID", FilterOperator.EQ, query)
                    ],false)]
                )}
            },

            countItems: function(oValue){
                if (oValue) {
                    var data = {name : oValue.mParameters.total}
                    var oModel = new JSONModel(data);
                    this.getView().setModel(oModel, "list");
                }
            },

            // View Setting Dialog opener
            _openDialog : function (sName, sPage, fInit) {
                var oView = this.getView();
                // creates requested dialog if not yet created
                if (!this._mDialogs[sName]) {
                    this._mDialogs[sName] = Fragment.load({
                        id: oView.getId(),
                        name: "desafioapp.view." + sName,
                        controller: this
                    }).then(function(oDialog){
                        oView.addDependent(oDialog);
                        //ainda inutilizado TODO
                        if (fInit) {
                            fInit(oDialog);
                        }
                        return oDialog;
                    });
                }
                this._mDialogs[sName].then(function(oDialog){
                    // opens the requested dialog
                    oDialog.open(sPage);
                });
            },

            // Opens View Settings Dialog on filter page
            handleOpenFilterDialog: function () {
                this._openDialog("DialogListViewSettings", "filter");
            },

            // Opens View Settings Dialog on group page
            handleOpenGroupDialog: function () {
                this._openDialog("DialogListViewSettings", "group");
            },

            handleViewSettingsConfirm: function(oEvent){
                const oTable = this.byId("orderList"),
                    oBinding = oTable.getBinding("items"),
                    mParams = oEvent.mParameters
                    

                const handleGroupingData = (oBinding, mParams)=>{
                    const bGroupDescending = mParams.groupDescending,
                        aGroups = []

                    if(mParams.groupItem){
                        const oGroupData = mParams.groupItem,
                            sPath = oGroupData.getKey()
                        
                        const fGetDataType = (oBinding)=>{
                            const allContexts = oBinding.getContexts()
                            
                            const aDataTypes = allContexts.map((context)=>{
                                const order = context.getProperty(sPath),
                                    orderType = typeof(order)
                                if (orderType == 'object' && order instanceof Date){
                                    return 'date'
                                }
                                return orderType
                            })

                            const countStrInArray = (aStrings) => {
                                let oReturn = {}
                                aStrings.forEach(sStr => {
                                    oReturn[sStr] = (oReturn[sStr]+1) || 1 
                                })
                                return oReturn
                            }

                            const getMaxProperty = (oCount) => {
                                let sMaxProp, iPropCount = -Infinity

                                Object.entries(oCount).forEach(entry=>{
                                    if(entry[1] > iPropCount){
                                        sMaxProp = entry[0]
                                        iPropCount = entry[1]
                                    }
                                })
                                return sMaxProp
                            }

                            switch(sPath){
                                case("Added/StatusText"):
                                    return "status"
                                default:
                                    const oStringCount = countStrInArray(aDataTypes)
                                    return getMaxProperty(oStringCount)
                            }
                            
                        }

                        const fGetCompareFunc = (sDataType) => {
                            const fStatusComparator = (a, b) => {
                                /**
                                 * usar a key 0 aqui faz o short circuit abaixo não funcionar
                                 * pois 0 vai ser lido como false
                                 * */ 
                                const DataDict = {
                                    "TOO LATE": 1,
                                    "URGENT": 2,
                                    "IN TIME": 3
                                }
                                // se for possível traduzir pelo DataDict o faz, se n, mantém
                                a = DataDict[a] || a
                                b = DataDict[b] || b

                                switch(true){
                                    case(a == b):
                                        return 0

                                    case(a < b):
                                        return -1;

                                    case(a > b):                                      
                                        return 1;

                                    case(a == null):
                                        return 1

                                    case(b == null):
                                        return -1

                                    default:
                                        return 0
                                }
                            }
                            
                            const fDefaultComparator = (a, b) => {
                                switch(true){
                                    /**
                                     * Inverte o retorno dos casos (a == null) e (b == null)
                                     * em comparação com o padrão da SAP para  os valores nulos
                                     * ficarem no fim
                                     */
                                    case(a == b):
                                        return 0

                                    case(a < b):
                                        return -1;

                                    case(a > b):
                                        return 1;

                                    case(a == null):
                                        return 1

                                    case(b == null):
                                        return -1

                                    case(typeof a == "string" && typeof b == "string"):
                                        return a.localeCompare(b);

                                    default:
                                        return 0
                                }
                            }

                            switch(sDataType){ 
                                case("status"):
                                return fStatusComparator
                                default:
                                    return fDefaultComparator
                            }
                        }

                        const fGetGroupFunc = (oBinding) => {

                            const fDateGrouper = (oContext)=>{
                                return oContext.getProperty(sPath).toLocaleDateString()
                            }

                            const fDefaultGrouper = (oContext)=>{
                                return oContext.getProperty(sPath)
                            }

                            switch(sDataType){ 
                                case("date"):
                                    return fDateGrouper
                                default:
                                    return fDefaultGrouper
                            }
                        }
                        
                        const sDataType = fGetDataType(oBinding),
                                fGrouper = fGetGroupFunc(sDataType),
                                fComparator = fGetCompareFunc(sDataType)

                        aGroups.push(new Sorter(sPath,
                                                bGroupDescending,
                                                fGrouper,
                                                fComparator
                                                ));
                        oBinding.sort(aGroups)
                    } else if (bGroupDescending){
                    // TODO dar um sort padrão aqui com BGoupDescending
                    }
                }
                
                handleGroupingData(oBinding, mParams)
                /**
                 * this.byId("listInfo").setVisible(aFilters.length > 0);
                 * this.byId("listInfoLabel").setText(mParams.filterString);
                 */
            }
        });
    });