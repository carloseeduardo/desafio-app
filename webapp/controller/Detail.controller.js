var myFormatter = {
    getCurrencyStr: function(value) {
        return Number(value).toFixed(2)
    },
    calculateTotal: function(unitPrice, quantity) {
        var oNumber = Number(unitPrice) * quantity;
        return oNumber.toFixed(2);
    } 
};

sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History",
    "sap/ui/model/json/JSONModel"

], function (Controller, History, JSONModel) {
	"use strict";
	return Controller.extend("desafioapp.controller.Detail", {
		onInit: function () {
			var oRouter = this.getOwnerComponent().getRouter();
            oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);
        },
        
		_onObjectMatched: function (oEvent) {
            var dialog = new sap.m.BusyDialog({
                text:'Loading Data...'
            });
            dialog.open();
            

            const orderID = oEvent.getParameter("arguments").orderPath;

            this.getView().bindElement({
				path: "/" + window.decodeURIComponent(`Orders(${orderID})`),
                model: "orders"
            });

            const oModel = new sap.ui.model.odata.v2.ODataModel('/V3/Northwind/Northwind.svc');

            const detailsData = new Promise((resolve, reject) => {
                oModel.read(`/Orders(${orderID})`, {
                    urlParameters: {
                        $expand: 'Order_Details,Order_Details/Product,Employee'
                    },
                    success: (response) => {
                        resolve(response);
                    }, error: (error) => {
                        reject(error);
                    }
                })
            })

            const subtotalsData = new Promise((resolve, reject) => {
                oModel.read(`/Order_Subtotals(${orderID})`, {
                    success: (response) => {
                        resolve(response.Subtotal);
                    }, error: (error) => {
                        reject(error);
                    }
                })
            })

            Promise.all([detailsData, subtotalsData])
                .then((result) => {
                    const data = result[0];
                    data.subtotals = result[1];
                    const detailsModel = new JSONModel(data);
                    this.getView().setModel(detailsModel, 'details');
                })
                .catch((error) => {
                    const oRouter = this.getOwnerComponent().getRouter();
                    oRouter.navTo('error');
                })
                .finally(() => {
                    dialog.close();
                });
        },

		onNavBack : function (oEvent){
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = this.getOwnerComponent().getRouter();
				oRouter.navTo("overview", {}, true);
            }

            const detailsModel = this.getView().getModel('details');
            detailsModel.setData(null);
        },

        countItems: function(oValue){
            if (oValue) {
                var data = {name : oValue.mParameters.total}
                var oModel = new JSONModel(data);
                this.getView().setModel(oModel, "table");
            }
        }
	});
});